﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetExam.Models
{
    public class Statut
    {
        public int ID { get; set; }

        [Required]
        public int IDStatut { get; set; }
        [Required]
        public string LibelleStatut { get; set; }
    }
}
