﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetExam.Models.ViewModel
{
    public class Vuemodele
    {
        public Licencie Personne { get; set; }

        public Statut Role { get; set; }
    }
}
