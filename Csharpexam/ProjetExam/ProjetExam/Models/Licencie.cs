﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetExam.Models
{
    public class Licencie
    {
        
        public int ID { get; set; }

        public int OwnerID { get; set; }

        [Required]
        public string Nom { get; set; }
        [Required]
        public string Prenom { get; set; }
        [Required]
        public int IDLicencie { get; set; }
    }
}
