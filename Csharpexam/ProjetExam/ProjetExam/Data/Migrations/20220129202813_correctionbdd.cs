﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetExam.Data.Migrations
{
    public partial class correctionbdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Licence",
                table: "Licence");

            migrationBuilder.RenameTable(
                name: "Licence",
                newName: "Licencie");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Licencie",
                table: "Licencie",
                column: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Licencie",
                table: "Licencie");

            migrationBuilder.RenameTable(
                name: "Licencie",
                newName: "Licence");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Licence",
                table: "Licence",
                column: "ID");
        }
    }
}
