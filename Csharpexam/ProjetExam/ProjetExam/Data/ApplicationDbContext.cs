﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjetExam.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetExam.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Licencie> Licencie { get; set; }
        public DbSet<ProjetExam.Models.Statut> Statut { get; set; }
    }
}
