﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.NationalitePage
{
    [Authorize(Roles = "Personnels")]
    public class DetailsModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public DetailsModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Nationalite Nationalite { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Nationalite = await _context.Nationalite.FirstOrDefaultAsync(m => m.ID == id);

            if (Nationalite == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
