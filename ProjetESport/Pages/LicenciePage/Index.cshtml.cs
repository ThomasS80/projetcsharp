﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.LicenciePage
{
    [Authorize(Roles = "Personnels, OrganisateurCompetition, Licencies")]
    public class IndexModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public IndexModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Licencie> Licencie { get;set; }
        public int LicencieStatutID;

        public async Task OnGetAsync()
        {
            Licencie = await _context.Licencie.Include(e => e.EquipeLicencie).ToListAsync();
        }
    }
}
