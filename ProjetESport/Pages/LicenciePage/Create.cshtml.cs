﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.LicenciePage
{
    [Authorize(Roles = "Personnels")]
    public class CreateModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public CreateModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet() //Permet de mettre un licencie dans l equipe
        {
            ViewData["EquipeLicencieID"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
            return Page();
        }

        [BindProperty]
        public Licencie Licencie { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Licencie.Add(Licencie);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
