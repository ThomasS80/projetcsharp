﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.RencontrePage
{
    public class IndexModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public IndexModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Rencontre> Rencontre { get;set; }

        public async Task OnGetAsync()
        {
            Rencontre = await _context.Rencontre.Include(e => e.Vainqueur).ToListAsync();
            Rencontre = await _context.Rencontre.Include(e => e.Perdant).ToListAsync();
            Rencontre = await _context.Rencontre.Include(e => e.JeuxChoisi).ToListAsync();
            Rencontre = await _context.Rencontre.Include(e => e.RencontreComp).ToListAsync();
        }
    }
}
