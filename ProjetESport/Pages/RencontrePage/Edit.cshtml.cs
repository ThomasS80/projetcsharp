﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.RencontrePage
{
    [Authorize(Roles = "Personnels")]
    public class EditModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public EditModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Rencontre Rencontre { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Rencontre = await _context.Rencontre.Include(r => r.IDEquipeVainq).FirstOrDefaultAsync(m => m.ID == id);

            Rencontre = await _context.Rencontre.Include(r => r.IDEquipePerd).FirstOrDefaultAsync(m => m.ID == id);

            Rencontre = await _context.Rencontre.Include(r => r.JeuxChoisi).FirstOrDefaultAsync(m => m.ID == id);

            Rencontre = await _context.Rencontre.Include(r => r.RencontreCompID).FirstOrDefaultAsync(m => m.ID == id);

            ViewData["Vainqueur"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
            ViewData["Perdant"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
            ViewData["JeuxID"] = new SelectList(_context.Jeux, "ID", "LibelleJeux");
            ViewData["RencontreComp"] = new SelectList(_context.Competition, "ID", "LibelleCompetition");

            if (Rencontre == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Rencontre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RencontreExists(Rencontre.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool RencontreExists(int id)
        {
            return _context.Rencontre.Any(e => e.ID == id);
        }
    }
}
