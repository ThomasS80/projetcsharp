﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.RencontrePage
{
    [Authorize(Roles = "Personnels")]
    public class CreateModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public CreateModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
         ViewData["IDEquipeVainq"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
         ViewData["IDEquipePerd"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
         ViewData["JeuxID"] = new SelectList(_context.Jeux, "ID", "LibelleJeux");
            return Page();
        }

        [BindProperty]
        public Rencontre Rencontre { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Rencontre.Add(Rencontre);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
