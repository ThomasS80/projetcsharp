﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.PageCompetition
{
    public class IndexModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public IndexModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Competition> Competition { get;set; }

        public async Task OnGetAsync()
        {
            Competition = await _context.Competition.Include(c => c.CompJeux).ToListAsync();
            Competition = await _context.Competition.Include(c => c.TypeComp).ToListAsync();
            Competition = await _context.Competition.Include(c => c.VainqueurCompetition).ToListAsync();
        }
    }
}
