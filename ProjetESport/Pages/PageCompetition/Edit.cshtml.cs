﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.PageCompetition
{
    [Authorize(Roles = "Personnels, OrganisateurCompetition")]
    public class EditModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public EditModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Competition Competition { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Competition = await _context.Competition.Include(e => e.TypeCompetitionID).FirstOrDefaultAsync(m => m.ID == id);

            Competition = await _context.Competition.Include(e => e.IDCompJeux).FirstOrDefaultAsync(m => m.ID == id);

            if (Competition == null)
            {
                return NotFound();
            }
            ViewData["CompJeux"] = new SelectList(_context.Jeux, "ID", "LibelleJeux");
            ViewData["TypeComp"] = new SelectList(_context.TypeComp, "ID", "LibelleTypeComp");
            ViewData["VainqueurCompetition"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Competition).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompetitionExists(Competition.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CompetitionExists(int id)
        {
            return _context.Competition.Any(e => e.ID == id);
        }
    }
}
