﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.PageCompetition
{
    [Authorize(Roles = "Personnels, OrganisateurCompetition")]
    public class CreateModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public CreateModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet() //Permet de choisir la nationalite et le jeu de l'equipe créée
        {
            ViewData["IDCompJeux"] = new SelectList(_context.Jeux, "ID", "LibelleJeux");
            ViewData["TypeCompetitionID"] = new SelectList(_context.TypeComp, "ID", "LibelleTypeComp");
            ViewData["CompEquipeID"] = new SelectList(_context.Equipe, "ID", "LibelleEquipe");
            return Page();
        }

            [BindProperty]
        public Competition Competition { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Competition.Add(Competition);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
