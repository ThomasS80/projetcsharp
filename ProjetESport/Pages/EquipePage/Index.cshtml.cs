﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.EquipePage
{
    [Authorize(Roles = "Personnels, OrganisateurCompetition, Licencies")]
    public class IndexModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public IndexModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Equipe> Equipe { get;set; }

        public async Task OnGetAsync()
        {
            Equipe = await _context.Equipe.Include(e => e.NationaliteEquipe).ToListAsync();
            Equipe = await _context.Equipe.Include(e => e.Jeu).ToListAsync();
        }
    }
}
