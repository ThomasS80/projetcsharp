﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.TypeCompPage
{
    public class IndexModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public IndexModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<TypeComp> TypeComp { get;set; }

        public async Task OnGetAsync()
        {
            TypeComp = await _context.TypeComp.ToListAsync();
        }
    }
}
