﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.TypeCompPage
{
    public class DetailsModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public DetailsModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public TypeComp TypeComp { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeComp = await _context.TypeComp.FirstOrDefaultAsync(m => m.ID == id);

            if (TypeComp == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
