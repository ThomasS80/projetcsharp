﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.TypeCompPage
{
    public class DeleteModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public DeleteModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TypeComp TypeComp { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeComp = await _context.TypeComp.FirstOrDefaultAsync(m => m.ID == id);

            if (TypeComp == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeComp = await _context.TypeComp.FindAsync(id);

            if (TypeComp != null)
            {
                _context.TypeComp.Remove(TypeComp);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
