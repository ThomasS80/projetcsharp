﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjetESport.Data;
using ProjetESport.Models;

namespace ProjetESport.Pages.JeuxPage
{
    [Authorize(Roles = "Personnels")]
    public class DeleteModel : PageModel
    {
        private readonly ProjetESport.Data.ApplicationDbContext _context;

        public DeleteModel(ProjetESport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Jeux Jeux { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jeux = await _context.Jeux.FirstOrDefaultAsync(m => m.ID == id);

            if (Jeux == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jeux = await _context.Jeux.FindAsync(id);

            if (Jeux != null)
            {
                _context.Jeux.Remove(Jeux);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
