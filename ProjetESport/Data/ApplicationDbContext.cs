﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ProjetESport.Models;

namespace ProjetESport.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Equipe> Equipe { get; set; }
        public DbSet<Licencie> Licencie { get; set; }
        public DbSet<Jeux> Jeux { get; set; }
        public DbSet<Statut> Statut { get; set; }
        public DbSet<Nationalite> Nationalite { get; set; }
        public DbSet<Competition> Competition { get; set; }
        public DbSet<TypeComp> TypeComp { get; set; }
        public DbSet<Rencontre> Rencontre { get; set; }

    }
}
