﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetESport.Migrations
{
    public partial class Renouvellementbd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Jeux_JeuxID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_TypeComp_IDTypeCompID",
                table: "Competition");

            migrationBuilder.RenameColumn(
                name: "JeuxID",
                table: "Competition",
                newName: "TypeCompetitionIDID");

            migrationBuilder.RenameColumn(
                name: "IDTypeCompID",
                table: "Competition",
                newName: "IDCompJeuxID");

            migrationBuilder.RenameIndex(
                name: "IX_Competition_JeuxID",
                table: "Competition",
                newName: "IX_Competition_TypeCompetitionIDID");

            migrationBuilder.RenameIndex(
                name: "IX_Competition_IDTypeCompID",
                table: "Competition",
                newName: "IX_Competition_IDCompJeuxID");

            migrationBuilder.AddColumn<int>(
                name: "CompJeux",
                table: "Competition",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Jeux_IDCompJeuxID",
                table: "Competition",
                column: "IDCompJeuxID",
                principalTable: "Jeux",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_TypeComp_TypeCompetitionIDID",
                table: "Competition",
                column: "TypeCompetitionIDID",
                principalTable: "TypeComp",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Jeux_IDCompJeuxID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_TypeComp_TypeCompetitionIDID",
                table: "Competition");

            migrationBuilder.DropColumn(
                name: "CompJeux",
                table: "Competition");

            migrationBuilder.RenameColumn(
                name: "TypeCompetitionIDID",
                table: "Competition",
                newName: "JeuxID");

            migrationBuilder.RenameColumn(
                name: "IDCompJeuxID",
                table: "Competition",
                newName: "IDTypeCompID");

            migrationBuilder.RenameIndex(
                name: "IX_Competition_TypeCompetitionIDID",
                table: "Competition",
                newName: "IX_Competition_JeuxID");

            migrationBuilder.RenameIndex(
                name: "IX_Competition_IDCompJeuxID",
                table: "Competition",
                newName: "IX_Competition_IDTypeCompID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Jeux_JeuxID",
                table: "Competition",
                column: "JeuxID",
                principalTable: "Jeux",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_TypeComp_IDTypeCompID",
                table: "Competition",
                column: "IDTypeCompID",
                principalTable: "TypeComp",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
