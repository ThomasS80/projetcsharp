﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetESport.Migrations
{
    public partial class ChangementRencontreClef : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Competition_IDRencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Equipe_IDEquipePerdID",
                table: "Rencontre");

            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Equipe_IDEquipeVainqID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_IDEquipePerdID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_IDEquipeVainqID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_IDRencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropColumn(
                name: "IDEquipePerdID",
                table: "Rencontre");

            migrationBuilder.RenameColumn(
                name: "Vainqueur",
                table: "Rencontre",
                newName: "VainqueurID");

            migrationBuilder.RenameColumn(
                name: "RencontreComp",
                table: "Rencontre",
                newName: "RencontreCompID");

            migrationBuilder.RenameColumn(
                name: "Perdant",
                table: "Rencontre",
                newName: "PerdantID");

            migrationBuilder.RenameColumn(
                name: "IDRencontreCompID",
                table: "Rencontre",
                newName: "IDEquipeVainq");

            migrationBuilder.RenameColumn(
                name: "IDEquipeVainqID",
                table: "Rencontre",
                newName: "IDEquipePerd");

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_PerdantID",
                table: "Rencontre",
                column: "PerdantID");

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_RencontreCompID",
                table: "Rencontre",
                column: "RencontreCompID");

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_VainqueurID",
                table: "Rencontre",
                column: "VainqueurID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Competition_RencontreCompID",
                table: "Rencontre",
                column: "RencontreCompID",
                principalTable: "Competition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Equipe_PerdantID",
                table: "Rencontre",
                column: "PerdantID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Equipe_VainqueurID",
                table: "Rencontre",
                column: "VainqueurID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Competition_RencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Equipe_PerdantID",
                table: "Rencontre");

            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Equipe_VainqueurID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_PerdantID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_RencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_VainqueurID",
                table: "Rencontre");

            migrationBuilder.RenameColumn(
                name: "VainqueurID",
                table: "Rencontre",
                newName: "Vainqueur");

            migrationBuilder.RenameColumn(
                name: "RencontreCompID",
                table: "Rencontre",
                newName: "RencontreComp");

            migrationBuilder.RenameColumn(
                name: "PerdantID",
                table: "Rencontre",
                newName: "Perdant");

            migrationBuilder.RenameColumn(
                name: "IDEquipeVainq",
                table: "Rencontre",
                newName: "IDRencontreCompID");

            migrationBuilder.RenameColumn(
                name: "IDEquipePerd",
                table: "Rencontre",
                newName: "IDEquipeVainqID");

            migrationBuilder.AddColumn<int>(
                name: "IDEquipePerdID",
                table: "Rencontre",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_IDEquipePerdID",
                table: "Rencontre",
                column: "IDEquipePerdID");

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_IDEquipeVainqID",
                table: "Rencontre",
                column: "IDEquipeVainqID");

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_IDRencontreCompID",
                table: "Rencontre",
                column: "IDRencontreCompID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Competition_IDRencontreCompID",
                table: "Rencontre",
                column: "IDRencontreCompID",
                principalTable: "Competition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Equipe_IDEquipePerdID",
                table: "Rencontre",
                column: "IDEquipePerdID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Equipe_IDEquipeVainqID",
                table: "Rencontre",
                column: "IDEquipeVainqID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
