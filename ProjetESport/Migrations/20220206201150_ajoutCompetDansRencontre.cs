﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetESport.Migrations
{
    public partial class ajoutCompetDansRencontre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IDRencontreCompID",
                table: "Rencontre",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RencontreComp",
                table: "Rencontre",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rencontre_IDRencontreCompID",
                table: "Rencontre",
                column: "IDRencontreCompID");

            migrationBuilder.AddForeignKey(
                name: "FK_Rencontre_Competition_IDRencontreCompID",
                table: "Rencontre",
                column: "IDRencontreCompID",
                principalTable: "Competition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rencontre_Competition_IDRencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropIndex(
                name: "IX_Rencontre_IDRencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropColumn(
                name: "IDRencontreCompID",
                table: "Rencontre");

            migrationBuilder.DropColumn(
                name: "RencontreComp",
                table: "Rencontre");
        }
    }
}
