﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetESport.Migrations
{
    public partial class ChangementClefbd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Equipe_CompEquipeIDID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Jeux_IDCompJeuxID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_TypeComp_TypeCompetitionIDID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_CompEquipeIDID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_IDCompJeuxID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_TypeCompetitionIDID",
                table: "Competition");

            migrationBuilder.RenameColumn(
                name: "VainqueurCompetition",
                table: "Competition",
                newName: "VainqueurCompetitionID");

            migrationBuilder.RenameColumn(
                name: "TypeCompetitionIDID",
                table: "Competition",
                newName: "TypeCompetitionID");

            migrationBuilder.RenameColumn(
                name: "TypeComp",
                table: "Competition",
                newName: "TypeCompID");

            migrationBuilder.RenameColumn(
                name: "IDCompJeuxID",
                table: "Competition",
                newName: "IDCompJeux");

            migrationBuilder.RenameColumn(
                name: "CompJeux",
                table: "Competition",
                newName: "CompJeuxID");

            migrationBuilder.RenameColumn(
                name: "CompEquipeIDID",
                table: "Competition",
                newName: "CompEquipeID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_CompJeuxID",
                table: "Competition",
                column: "CompJeuxID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_TypeCompID",
                table: "Competition",
                column: "TypeCompID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_VainqueurCompetitionID",
                table: "Competition",
                column: "VainqueurCompetitionID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Equipe_VainqueurCompetitionID",
                table: "Competition",
                column: "VainqueurCompetitionID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Jeux_CompJeuxID",
                table: "Competition",
                column: "CompJeuxID",
                principalTable: "Jeux",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_TypeComp_TypeCompID",
                table: "Competition",
                column: "TypeCompID",
                principalTable: "TypeComp",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Equipe_VainqueurCompetitionID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_Jeux_CompJeuxID",
                table: "Competition");

            migrationBuilder.DropForeignKey(
                name: "FK_Competition_TypeComp_TypeCompID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_CompJeuxID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_TypeCompID",
                table: "Competition");

            migrationBuilder.DropIndex(
                name: "IX_Competition_VainqueurCompetitionID",
                table: "Competition");

            migrationBuilder.RenameColumn(
                name: "VainqueurCompetitionID",
                table: "Competition",
                newName: "VainqueurCompetition");

            migrationBuilder.RenameColumn(
                name: "TypeCompetitionID",
                table: "Competition",
                newName: "TypeCompetitionIDID");

            migrationBuilder.RenameColumn(
                name: "TypeCompID",
                table: "Competition",
                newName: "TypeComp");

            migrationBuilder.RenameColumn(
                name: "IDCompJeux",
                table: "Competition",
                newName: "IDCompJeuxID");

            migrationBuilder.RenameColumn(
                name: "CompJeuxID",
                table: "Competition",
                newName: "CompJeux");

            migrationBuilder.RenameColumn(
                name: "CompEquipeID",
                table: "Competition",
                newName: "CompEquipeIDID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_CompEquipeIDID",
                table: "Competition",
                column: "CompEquipeIDID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_IDCompJeuxID",
                table: "Competition",
                column: "IDCompJeuxID");

            migrationBuilder.CreateIndex(
                name: "IX_Competition_TypeCompetitionIDID",
                table: "Competition",
                column: "TypeCompetitionIDID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Equipe_CompEquipeIDID",
                table: "Competition",
                column: "CompEquipeIDID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_Jeux_IDCompJeuxID",
                table: "Competition",
                column: "IDCompJeuxID",
                principalTable: "Jeux",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Competition_TypeComp_TypeCompetitionIDID",
                table: "Competition",
                column: "TypeCompetitionIDID",
                principalTable: "TypeComp",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
