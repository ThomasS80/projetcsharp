﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class Competition
    {
        public int ID { get; set; }
        public int CompetitionID { get; set; }
        [Required]
        public string LibelleCompetition { get; set; }
        public int NbEquipe { get; set; }
        public int? CompEquipeID { get; set; }
        // Lien de navigation
        public Equipe VainqueurCompetition
        {
            get; set;
        }
        public int? IDCompJeux { get; set; }
        //Lien de navigation
        public Jeux CompJeux
        {
            get; set;
        }
        public int? TypeCompetitionID { get; set; }
        //Lien de navigation
        public TypeComp TypeComp
        {
            get; set;
        }
    }
}
