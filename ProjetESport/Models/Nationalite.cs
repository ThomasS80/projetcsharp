﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class Nationalite
    {
        public int ID { get; set; }
        public int IDNationalite { get; set; }
        [Required]
        public string LibelleNationalite { get; set; }
    }
}
