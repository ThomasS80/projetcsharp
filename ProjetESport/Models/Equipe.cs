﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class Equipe
    {
        public int ID { get; set; }
        public int EquipeID { get; set; }
        [Required]
        public string LibelleEquipe { get; set; }
        public int PointsEquipe { get; set; }

        public ICollection<Licencie> LicencieInscrits { get; set; }
        public int? NationaliteEquipeID { get; set; }
        // Lien de navigation
        public Nationalite NationaliteEquipe
        {
            get; set;
        }
        public int? JeuxID { get; set; }
        //Lien de navigation
        public Jeux Jeu
        {
            get; set;
        }
    }
}
