﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class TypeComp
    {
        public int ID { get; set; }
        public int TypeCompID { get; set; }
        [Required]
        public string LibelleTypeComp { get; set; }
    }
}
