﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class Rencontre
    {
        public int ID { get; set; }
        public int RencontreID { get; set; }
        public string DateRencontre { get; set; }
        public int? IDEquipeVainq { get; set; }
        // Lien de navigation
        public Equipe Vainqueur
        {
            get; set;
        }
        public int? IDEquipePerd { get; set; }
        // Lien de navigation
        public Equipe Perdant
        {
            get; set;
        }
        public int? JeuxID { get; set; }
        //Lien de navigation
        public Jeux JeuxChoisi
        {
            get; set;
        }
        public int? RencontreCompID { get; set; }
        //Lien de navigation
        public Competition RencontreComp
        {
            get; set;
        }

    }
}
