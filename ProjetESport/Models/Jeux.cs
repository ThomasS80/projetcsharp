﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetESport.Models
{
    public class Jeux
    {
        public int ID { get; set; }
        public int IDJeux { get; set; }
        [Required]
        public string LibelleJeux { get; set; }
    }
}
